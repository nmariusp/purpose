# Simon Depiets <sdepiets@gmail.com>, 2018.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: purpose_youtube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-03 02:07+0200\n"
"PO-Revision-Date: 2021-01-14 16:02+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.12.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: youtubejobcomposite.cpp:40
#, kde-format
msgid "No YouTube account configured in your accounts."
msgstr "Aucun compte « YouTube » n'est configuré au sein de vos comptes."

#: youtubeplugin_config.qml:30
#, kde-format
msgid "Account:"
msgstr "Compte :"

#: youtubeplugin_config.qml:52
#, kde-format
msgid "Title:"
msgstr "Titre : "

#: youtubeplugin_config.qml:56
#, kde-format
msgid "Enter a title for the video..."
msgstr "Saisissez un titre pour la vidéo…"

#: youtubeplugin_config.qml:59
#, kde-format
msgid "Tags:"
msgstr "Étiquettes :"

#: youtubeplugin_config.qml:63
#, kde-format
msgid "KDE, Kamoso"
msgstr "KDE, Kamoso"

#: youtubeplugin_config.qml:66
#, kde-format
msgid "Description:"
msgstr "Description :"
