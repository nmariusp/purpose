msgid ""
msgstr ""
"Project-Id-Version: purpose_phabricator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:42+0000\n"
"PO-Revision-Date: 2018-05-29 12:40+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: arc Purpose Phabricator\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: phabricatorjobs.cpp:59 phabricatorjobs.cpp:195
#, kde-format
msgid "Could not find the 'arc' command"
msgstr "Não foi possível encontrar o comando 'arc'"

#: phabricatorjobs.cpp:117
#, kde-format
msgid "Could not create the new \"differential diff\""
msgstr "Não foi possível criar a nova \"versão diferencial\""

#: phabricatorjobs.cpp:154
#, kde-format
msgid "Patch upload to Phabricator failed"
msgstr "Falhou o envio da modificação para o Phabricator"

#: phabricatorjobs.cpp:206
#, kde-format
msgid "Could not get list of differential revisions in %1"
msgstr "Não foi possível obter a lista de revisões diferenciais no %1"

#: phabricatorplugin.cpp:49
#, kde-format
msgid "Phabricator refuses empty patchfiles"
msgstr "O Phabricator recusa ficheiros de modificações vazios"

#: phabricatorplugin.cpp:54 phabricatorplugin_config.qml:55
#, kde-format
msgid "unknown"
msgstr "desconhecido"

#: phabricatorplugin.cpp:56
#, kde-format
msgid ""
"Please choose between creating a new revision or updating an existing one"
msgstr ""
"Escolha por favor se deseja criar uma nova revisão ou actualizar uma "
"existente"

#: phabricatorplugin_config.qml:28
#, kde-format
msgid "Update differential revision %1"
msgstr "Actualizar a revisão diferencial %1"

#: phabricatorplugin_config.qml:28
#, kde-format
msgid "Update differential revision"
msgstr "Actualizar a revisão diferencial"

#: phabricatorplugin_config.qml:30
#, kde-format
msgid "Create new \"differential diff\""
msgstr "Criar uma nova \"versão diferencial\""

#: phabricatorplugin_config.qml:32
#, kde-format
msgid "Create or update?"
msgstr "Criar ou actualizar?"

#: phabricatorplugin_config.qml:65
#, kde-format
msgid "New Diff"
msgstr "Novas Diferenças"

#: phabricatorplugin_config.qml:66
#, kde-format
msgid ""
"tick this to create a new \"differential diff\" which can\n"
"be converted online to a new differential revision"
msgstr ""
"assinale isto para criar uma nova \"versão diferencial\" que possa\n"
"ser convertida 'online' para uma nova revisão diferencial"

#: phabricatorplugin_config.qml:73
#, kde-format
msgid "Update Diff"
msgstr "Actualizar as Diferenças"

#: phabricatorplugin_config.qml:74
#, kde-format
msgid ""
"tick this to update an existing revision,\n"
"select one from the list below."
msgstr ""
"assinale isto para actualizar uma revisão existente,\n"
"seleccionando uma na lista abaixo."

#: phabricatorplugin_config.qml:102
#, kde-format
msgid "Open Diff in browser"
msgstr "Abrir as Diferenças no Navegador"

#. i18nd("purpose_phabricator", ).arg() to avoid showing the "%1" when inactive
#: phabricatorplugin_config.qml:109
#, kde-format
msgid "Summary of the update to %1:"
msgstr "Resumo da actualização de %1:"

#: phabricatorplugin_config.qml:109
#, kde-format
msgid "Summary of the update"
msgstr "Resumo da actualização"

#: phabricatorplugin_config.qml:117
#, kde-format
msgid "patch updated through %1 and the Purpose/Phabricator plugin"
msgstr ""
"modificação actualizada através do %1 e do 'plugin' do Purpose/Phabricator"

#~ msgid "Please note"
#~ msgstr "Não se esqueça"

#~ msgid "Remember to complete the differential revision online!"
#~ msgstr "Lembre-se de completar a revisão diferencial 'online'!"
