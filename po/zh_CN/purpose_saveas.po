msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:42+0000\n"
"PO-Revision-Date: 2022-12-01 06:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/purpose/purpose_saveas.pot\n"
"X-Crowdin-File-ID: 6843\n"

#: saveasplugin.cpp:45
#, kde-format
msgid "No URLs to save"
msgstr "没有要保存的 URL"

#: saveasplugin_config.qml:19
#, kde-format
msgid "Save directory:"
msgstr "保存目录："

#: saveasplugin_config.qml:32
#, kde-format
msgid "Select a directory where to save your pictures and videos"
msgstr "选择保存图片和视频的目录"
